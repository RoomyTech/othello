﻿using UnityEngine;
using System.Collections;

namespace Utility
{

	public static class RectTransformExtesions
	{

		public static RectTransform SetAnchorX (this RectTransform self, float x)
		{
			var anchorPos = self.anchoredPosition;
			anchorPos.x = x;
			self.anchoredPosition = anchorPos;
			return self;
		}

		public static RectTransform SetAnchorY (this RectTransform self, float y)
		{
			var anchorPos = self.anchoredPosition;
			anchorPos.y = y;
			self.anchoredPosition = anchorPos;
			return self;
		}

		public static RectTransform SetAnchorPos (this RectTransform self, float x, float y)
		{
			self.anchoredPosition = new Vector2 (x, y);
			return self;
		}

		public static T SetRotateX<T> (this T self, float x) where T : Transform
		{
			var rotation = self.localRotation;
			rotation.x = x;
			self.localRotation = rotation;
			return self;
		}

		public static T SetRotateY<T> (this T self, float y) where T : Transform
		{
			var rotation = self.localRotation;
			rotation.y = y;
			self.localRotation = rotation;
			return self;
		}

		public static T SetRotateZ<T> (this T self, float z) where T :Transform
		{
			var rotation = self.localRotation;
			rotation.z = z;
			self.localRotation = rotation;
			return self;
		}


		public static T SetScaleX<T> (this T self, float x) where T : Transform
		{
			var scale = self.localScale;
			scale.x = x;
			self.localScale = scale;
			return self;
		}

		public static T SetScaleY<T> (this T self, float y) where T : Transform
		{
			var scale = self.localScale;
			scale.y = y;
			self.localScale = scale;
			return self;
		}

		public static T SetScaleZ<T> (this T self, float z) where T : Transform
		{
			var scale = self.localScale;
			scale.z = z;
			self.localScale = scale;
			return self;
		}

		public static RectTransform SetScaleXY (this RectTransform self, float x, float y)
		{
			var scale = self.localScale;
			scale.x = x;
			scale.y = y;
			self.localScale = scale;
			return self;
		}

	}

}