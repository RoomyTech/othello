﻿using System.Collections.Generic;
using System.Linq;
using System;


namespace Utility
{

	public static class ListExtensions
	{

		/// <summary>
		/// Finds the value.
		/// </summary>
		/// <returns>The value.</returns>
		/// <param name="self">Self.</param>
		/// <param name="index">Index.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T FindValue<T> (this List<T> self, int index)
		{

			try { 
				return self [index];
			} catch (System.Exception) {
			}
			return self.FirstOrDefault ();
		}


		public static string toString<T> (this IEnumerable<T> self)
		{
			return string.Join (",", Array.ConvertAll (self.ToArray (), p => p.ToString ()));
		}
	}

}