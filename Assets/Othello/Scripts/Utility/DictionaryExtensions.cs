﻿using System.Collections.Generic;
using System.Linq;


namespace Utility
{

	/// <summary>
	/// Dictionary extensions.
	/// </summary>
	public static class DictionaryExtensions
	{
		/// <summary>
		/// Finds the key.
		/// </summary>
		/// <returns>The key.</returns>
		/// <param name="self">Self.</param>
		/// <param name="key">Key.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		/// <typeparam name="T2">The 2nd type parameter.</typeparam>
		public static T2 FindValue<T, T2> (this Dictionary<T,T2> self, T key)
		{
            
			try {
				T2 result = self [key];
				return result;
			} catch (System.Exception) {
                
			}
			//最初のValueをとりあえず返す。何か入ってれば何かは返る
			return self.Values.FirstOrDefault ();
		}
	}

}