﻿using UnityEngine;
using System.Collections.Generic;

using UniRx;
using System.Linq;

namespace Othello
{

    public enum CELL_STATE
    {
        OUT_INDEX = -1,
        NONE = 0,
        BLACK = 1,
        WHITE = 2,

    }


    [System.Serializable]
    enum GameTurn
    {
        GAME_START = 0,
        BLACK = 1,
        WHITE = 2,
        GAME_END = 3,
    }

    [System.Serializable]
    public class UpdateCellInfo
    {
        ///<summary> 更新セル </summary>
        public int index;
        ///<summary> 更新状態 </summary>
        public CELL_STATE state;
    }



    public static class Extension
    {

        public static string toString<T>(this IEnumerable<T> self)
        {
            return string.Join(",", System.Array.ConvertAll<T, string>(self.ToArray(), o => o.ToString()));
        }
    }


    public class GameManager : MonoBehaviour
    {




        static readonly int[] board_data = new int[] {
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 2, 1, 0, 0, 0,
            0, 0, 0, 1, 2, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
        };


        static readonly int WIDTH = 8;
        static readonly int HEIGHT = 8;


        [SerializeField]
        CELL_STATE[] board = null;
        [SerializeField]
        GameTurn turn = GameTurn.GAME_START;
        CELL_STATE[] beforeBoard = new CELL_STATE[WIDTH * HEIGHT];

        Subject<UpdateCellInfo[]> updateSubject = new Subject<UpdateCellInfo[]>();
        List<UpdateCellInfo> updateList = new List<UpdateCellInfo>();

        public void Initialize()
        {
            board = board_data.Select(d => (CELL_STATE)d).ToArray();
        }

        /// <summary>
        /// 指定のindexに石を置く
        /// </summary>
        /// <param name="index">Index.</param>
        public void SetStone(int x, int y)
        {
            if (turn == GameTurn.GAME_START || turn == GameTurn.GAME_END)
            {
                Debug.Log("SetStone is invalid current turn is " + turn);
                return;
            }




            board.CopyTo(beforeBoard, 0);
            board[x + y * WIDTH] = turn == GameTurn.BLACK ? CELL_STATE.BLACK : CELL_STATE.WHITE;

            //挟んでいる箇所があるか全方位判定する
            CheckCells(x, y);


            Debug.Log("before " + beforeBoard.toString());
            Debug.Log("after " + board.toString());

            updateList.Clear();
            for (int i = 0; i < WIDTH*HEIGHT; i++)
            {
                if (board[i] != beforeBoard[i])
                {
                    updateList.Add(new UpdateCellInfo()
                    {
                        index = i,
                        state = board[i]
                    });
                }
            }

            updateSubject.OnNext(updateList.ToArray());

            UpdateTurn();
        }


        public void CheckCells(int x, int y)
        {
            //現在の場所の色
            CELL_STATE state = Cell(x, y);
            //上判定
            CELL_STATE ue = Cell(x, y - 1);

        }


        public CELL_STATE Cell(int x, int y)
        {
            if (x < 0 || y < 0 || x > WIDTH || y > HEIGHT) return CELL_STATE.OUT_INDEX;

            return board[x + y * WIDTH];
        }

        public IObservable<UpdateCellInfo[]> UpdateCellsAsObservable()
        {
            return updateSubject.AsObservable();
        }

        void UpdateTurn()
        {
            turn = turn == GameTurn.BLACK ? GameTurn.WHITE : GameTurn.BLACK;
        }


        public CELL_STATE[] BoardData()
        {
            return board;
        }


        /// <summary>
        /// ゲーム開始処理
        /// </summary>
        public void GameStart()
        {
            turn = GameTurn.BLACK;

        }
    }

}

