﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;


namespace Othello
{

    public class Cell : MonoBehaviour
    {


        [SerializeField]
        CELL_STATE state = CELL_STATE.NONE;
        [SerializeField]
        Image white = null;
        [SerializeField]
        Image black = null;

        [SerializeField]
        int cellX = 0;
        [SerializeField]
        int cellY = 0;

        Subject<Cell> clickSubject = new Subject<Cell>();

        public IObservable<Cell> OnClickCellAsObservable()
        {
            return clickSubject.AsObservable();
        }


        /// <summary>
        /// クリック呼び出し　Inspector設定
        /// </summary>
        public void Click()
        {
            if (state == CELL_STATE.BLACK || state == CELL_STATE.WHITE)
                return;
            clickSubject.OnNext(this);
        }


        public void SetCellState(CELL_STATE cell)
        {
            state = cell;
            switch (cell)
            {
                case CELL_STATE.BLACK:
                    black.enabled = true;
                    white.enabled = false;
                    break;
                case CELL_STATE.WHITE:
                    white.enabled = true;
                    black.enabled = false;
                    break;
                case CELL_STATE.NONE:
                default:
                    black.enabled = false;
                    white.enabled = false;
                    break;
            }

            gameObject.name = cell.ToString();
        }

        public void SetCellIndex(int x, int y)
        {
            cellX = x;
            cellY = y;
        }

        public int CellX() { return cellX; }
        public int CellY() { return cellY; }

        void OnDestroy()
        {
            clickSubject.OnCompleted();
        }
    }

}

