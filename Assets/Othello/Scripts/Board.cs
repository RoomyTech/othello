﻿using UnityEngine;
using System.Collections;
using Unity.Linq;
using System.Linq;
using UniRx;

namespace Othello
{

    public class Board : MonoBehaviour
    {


        [SerializeField]
        GameObject boardCell = null;
        [SerializeField]
        GameManager gameLogic = null;
        [SerializeField]
        Cell[] cells = new Cell[64];

        void Start()
        {


            gameLogic.Initialize();

            BoardInitialize();

            //セルのクリック
            CellClickObserver();


            CellUpadteObserver();

            gameLogic.GameStart();
        }


        void BoardInitialize()
        {

            int x = 0, y = 0;
            for (int i = 0; i < gameLogic.BoardData().Length; i++)
            {
                var cell = gameObject.Add(boardCell).GetComponent<Cell>();
                cell.GetComponent<RectTransform>().anchoredPosition = new Vector2(x * 64, -y * 64);

                x++;
                if (x >= 8)
                {
                    x = 0;
                    y++;
                }
                cell.SetCellState(gameLogic.BoardData()[i]);
                cell.SetCellIndex(x, y);
                cells[i] = cell;
            }

        }


        void CellClickObserver()
        {


            Observable.Merge(cells.Select(c => c.OnClickCellAsObservable()))
                .Subscribe(cell =>
                {

                    Debug.Log("cell update");
                    gameLogic.SetStone(cell.CellX(), cell.CellY());
                }).AddTo(this);

        }

        void CellUpadteObserver()
        {

            gameLogic.UpdateCellsAsObservable()
                .Subscribe(updates =>
                {
                    foreach (var update in updates)
                    {
                        cells[update.index].SetCellState(update.state);
                    }
                }).AddTo(this);

        }


    }
}

