﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UniRx;

public class PublishLast : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{


//		testAsync ()
//			.Subscribe ();



//		var pub1 = testPublishAsync ();
//		var pub = pub1.Subscribe (_ => Debug.Log ("pub1 subscribe"));
//		var pub2 = pub1.Subscribe (_ => Debug.Log ("pub1 subscribe2"));
//
//
//		Observable.Delay (Observable.ReturnUnit (), System.TimeSpan.FromSeconds (1))
//			.Subscribe (_ => {
//			pub1.Subscribe (u => Debug.Log ("pub1 subscribe3"));
//			Debug.Log ("delay pub1");
//		});

//		method = testPublishLastAsync
//			publishLast subscribe
//			publishLast subscribe2
//			publishLast subscribe3
//			method = testPublishLastAsync
//			delay publast1
//			cancel.IsCancellationRequested true : testPublishLastAsync
//

		var pubLas1 = testPublishLastAsync ();
		var publishLast = pubLas1.Subscribe (_ => Debug.Log ("publishLast subscribe"));
		var publishLast2 = pubLas1.Subscribe (_ => Debug.Log ("publishLast subscribe2"));
//		publishLast2.Dispose ();
//		publishLast.Dispose ();

		Observable.Delay (Observable.ReturnUnit (), System.TimeSpan.FromSeconds (3))
			.Subscribe (_ => {
			pubLas1.Subscribe (u => Debug.Log ("publishLast subscribe3"));
				pubLas1.Subscribe (u => Debug.Log ("publishLast subscribe4"));
			Debug.Log ("delay publast1");
		});

//		method = testPublishLastAsync
//			publishLast subscribe
//			publishLast subscribe2
//			publishLast subscribe3
//			method = testPublishLastAsync
//			delay publast1
//			cancel.IsCancellationRequested true : testPublishLastAsync
//

	}



	IObservable<Unit> testAsync ()
	{
		return Observable.FromCoroutine<Unit> ((observer, cancel) => test (observer, cancel, "testAsync"));
	}

	IObservable<Unit> testPublishLastAsync ()
	{
		return Observable.FromCoroutine<Unit> ((observer, cancel) => test (observer, cancel, "testPublishLastAsync")).PublishLast ().RefCount ();
	}

	IObservable<Unit> testPublishAsync ()
	{
		return Observable.FromCoroutine<Unit> ((observer, cancel) => test (observer, cancel, "testPublishAsync")).Publish ().RefCount ();
	}

	IEnumerator test (IObserver<Unit> observer, CancellationToken cancel, string method)
	{
		Debug.Log ("method = " + method);


		yield return new WaitForSeconds (2.0f);

		Debug.Log ("method = " + method + " wait 2.0f");
	
		if (cancel.IsCancellationRequested) {
			Debug.Log ("cancel.IsCancellationRequested true : " + method);
			yield break;
		}

		observer.OnNext (Unit.Default);


		observer.OnCompleted ();



	}
}
